#!/usr/bin/env python3
import os
import shutil
import time
from datetime import datetime

import docker

PODMAN_SOCK = os.environ.get("PODMAN_SOCK", "unix://run/podman.sock")
THRESHOLD = float(os.environ.get("THRESHOLD", 0.6))


podman = docker.DockerClient(base_url=PODMAN_SOCK)
local_images = {}

print("Populating initial state")
for image in podman.images.list():
    local_images[image.id] = {
        "last_used": datetime.strptime(
            image.attrs["Created"].split(".")[0].removesuffix("Z"), "%Y-%m-%dT%H:%M:%S"
        ).timestamp(),
        "size": image.attrs["Size"],
        "tags": image.attrs["RepoTags"],
    }

total, images_disk_usage, _ = shutil.disk_usage("/var/lib/containers")
usage_threshold = int(total * THRESHOLD)


def remove_lru_images(images_disk_usage, usage_threshold):
    print(f"Images are using {images_disk_usage}, which is more than {usage_threshold}")
    lru_images = sorted(local_images.items(), key=lambda x: x[1]["last_used"])
    protected_images = [
        "docker.io/gitlab/gitlab-runner:ubi-fips",
        "docker.io/gitlab/gitlab-runner:ubuntu",
        "docker.io/library/traefik:v2.10",
        "quay.io/gnome_infrastructure/pydocuum:latest",
        "quay.io/minio/minio:latest",
        "registry.gitlab.com/buildgrid/buildbox/buildbox-casd:latest",
        "registry.gitlab.com/gitlab-org/gitlab-runner:latest",
    ]
    for image in lru_images:
        try:
            is_protected = any(True for tag in image[1].get("tags") if tag in protected_images)
            if is_protected:
                print(f"Skipping protected image {image[0]}")
                continue
        except:
            is_protected = False

        print(f"Deleting image {image[0]}")
        podman.images.remove(image[0], force=True)
        del local_images[image[0]]
        _, images_disk_usage, _ = shutil.disk_usage("/var/lib/containers")
        if images_disk_usage < usage_threshold:
            print(
                f"Images are using {images_disk_usage}, which is within limit of {usage_threshold}"
            )
            break


while True:
    if images_disk_usage > usage_threshold:
        remove_lru_images(images_disk_usage, usage_threshold)

    print("Waiting for events")
    events = podman.events(decode=True)
    for event in events:
        print(event)
        if event.get("Type") == "container":
            if event.get("Action") == "start":
                if (
                    container_image := event.get("Actor", {})
                    .get("Attributes", {})
                    .get("image")
                ):
                    image = podman.images.get(container_image)

                    print(f"Updating timestamp for image {image.id}")
                    if image.id not in local_images:
                        local_images[image.id] = {
                            "last_used": datetime.now().timestamp(),
                            "size": image.attrs["Size"],
                        }
                    else:
                        local_images[image.id]["last_used"] = datetime.now().timestamp()

        _, images_disk_usage, _ = shutil.disk_usage("/var/lib/containers")
        if images_disk_usage > usage_threshold:
            remove_lru_images(images_disk_usage, usage_threshold)
        else:
            print(
                f"Images are using {images_disk_usage}, which is within limit of {usage_threshold}"
            )
