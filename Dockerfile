FROM registry.access.redhat.com/ubi9/python-311:1

USER 0
RUN pip install docker
ADD main.py /usr/local/bin/pydocuum
ENTRYPOINT ["/usr/local/bin/pydocuum"]
